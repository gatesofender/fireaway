package org.projectapex.FireAway;

import org.bukkit.Bukkit;
import org.bukkit.configuration.Configuration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

class FireWorkListener implements Listener {
	
    private final FireWorkPlugin plugin;

    private FireWorkListener() {
        this.plugin = null;
    }


    public FireWorkListener(final FireWorkPlugin plugin) {
        Preconditions.checkArgument(Preconditions.checkNotNull(plugin, "Plugin").isEnabled(), "Plugin not loaded.");
        this.plugin = plugin;
    }

    // TODO onPlayerInteract Listener

}

