package org.projectapex.FireAway;

import org.bukkit.Bukkit;
import org.bukkit.configuration.Configuration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

class Main extends JavaPlugin {
	private Set<Listener> listeners;


    @Override
    public void onEnable() {
    	// todo properly enable

        this.listeners = new HashSet<>();
        FireWorkListener fireListener = new FireWorkListener(this);
        for (Listener listener : this.listeners) {
            Bukkit.getPluginManager().registerEvents(listener, this);
        }

        // todo permissions

    }

    @Override
    public void onDisable() {
    	// todo properly disable

    	for (Listener listener : this.listeners) {
            if (listener instanceof FireWorkListener) {
                ((FireWorkListener) listener).unRegisterAll();
            }
            HandlerList.unregisterAll(listener);
        }


    }
}

